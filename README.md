# IHTAPark
![](/Resources/Icon128.png)
## Summary
The IHTAPark Plugin contains a 3D model of the 'IHTA Park', the lawn in front of the Institute for Hearing Technology and Acoustics at RWTH Aachen University.
This model can be used as a building block to create demos such as listening experiments in Unreal Engine.
## Content
IHTAPark provides **nine** different versions of the IHTA Park model: three weather conditions (Summer, Autumn, Winter) with three levels of detail each.
### Levels of Detail
1. **High** level of detail without any alterations
2. **Medium** level of detail with disabled wind animations for vegetation as well as removed windows from buildings
3. **Low** level of detail with all of the above as well as reducing distant buildings to single cubes and removing grass